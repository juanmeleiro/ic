# Rendering

1. Run `./prepare` to inject all code in \*.idr into respective \*.tex
2. Run `xelatex -shell-escape main.tex` as many times as needed
