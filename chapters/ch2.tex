\documentclass[../main.tex]{subfiles}

\begin{document}

    \chapter{O que é uma lógica?}

    \section{À literatura}

        O ponto de partida mais natural para uma pergunta definicional como essa
        é a revisão bibliográfica. Esperavamos encontrar, dentre a miríade de
        definições estabelecidas de \emph{lógicas}, uma que se encaixasse com a
        nossa exploração.

        Embora o estudo da lógica -- que, apesar da uniformidade de uso dessa
        palavra, não é um campo homogêneo -- é quase tão antigo quanto o
        pensamento ocidental \note{referência}. No entanto, não parece haver
        nenhum tipo de consenso quando se trata de definir o objeto de estudo
        dessa disciplina. Não há definição única da palavra ``lógica''.

        Mesmo assim, existem diversas definições com níveis de generalizade
        variados. Começando em \note{época do tarski}, existem os conhecidos
        \strong{operadores de Tarski}, formulados pelo próprio como uma forma de
        \note{preencher}. Chamam a atenção por serem uma formulação
        ``universal'': uma lógica, por assim dizer, seria dada por um conjunto
        de dados (o conjunto de formulas e o operador, ou relação) satisfazendo
        certas propriedades (como finitariedade, monotonicidade, reflexividade).
        Talvez não propriedades universais, de fato, mas a definição contrasta
        com alguma mais construtiva que veremos mais adiante.

        \note{Texto corrido sobre outras definições}

        Em resumo, consideramos as seguintes definições e interpretações
        relevantes:

        \begin{description}

            \item[Tarski] A ideia de uma lógica como uma formulação “universal”:
                uma estrutura opaca que satisfaz propriedades. No entanto, não
                especifíca nenhum tipo de teoria da prova, não é construtiva.

            \item[Instituições] Lógicas formam uma categoria com as suas
                traduções. No entanto, é uma formulação modelo-teórica,
                inadequada para um teorema prova-teórico.

            \item[Lambek] Lógicas são categorias em si, ou categorias com
                estrutura adicional. Infelizmente, as categorias consideradas
                por \cite{LambekCategorical1988} são restritas a lógicas
                ``tradicionais'', com conjunções, disjuções, \foreign{etc}.

            \item[Sistemas Dedutivos] Dedução como prova. Um sistema lógica é
                uma especificação de como se pode construir uma prova
                \note{Relação com Martin Löf e BHK}. Um prova é uma regra,
                abstrata, aplicada a provas de cada uma de suas hipóteses.

        \end{description}

    \section{Relações com o que já há}

        Testar definições pode significar compará-las e encontrar traduções a
        definições similares, melhor estabelecidas. Se vamos trabalhar com
        sistemas dedutivos, de que forma se enquadram como relações dedutivas
        \foreign{a là} Tarski?

        A primeira observação relevante é que um sistema dedutivo, assim,
        abstratamente, não é uma teoria de \emph{inferência}, tanto quanto é uma
        teoria de \emph{prova}. Afinal, a definição não é de provas de algo
        \emph{sob certas hipóteses}. Para isso, podemos especificar o tipo de
        julgamentos usados. Ao invés de considerar uma classe abstrata de
        julgamentos, consideramos uma classe de julgamentos \strong{hipotéticos}
        sobre uma classe base.

        \begin{definition}[Inferência]

            Uma \strong{inferência} sobre uma classe de julgamentos
            $\mathcal{J}$ é um objeto da forma

            \[
                J_0, \cdots, J_{n-1} \vdash J
            \]

            com $J_i, J : \mathcal{J}$. Denotamos a classe de julgamentos dessa
            forma como $\mathrm{Inference}\;\mathcal{J}$.

        \end{definition}

        \begin{definition}
            Um \strong{sistema dedutivo hipotético} sobre uma classe
            $\mathcal{J}$ de julgamentos é um sistema dedutivo sobre
            $\mathrm{Inference}\;\mathcal{J}$
        \end{definition}

        Agora fica evidente a construção da relação que buscamos: um sistema
        dedutivo hipotético induz uma relação dedutiva dizendo que certa
        inferência é válida se há prova dela.

        Mas aqui encontramos um problema. A definição de inferência postula as
        hipóteses como uma lista finita de julgamentos, que é usual e,
        ocasionalmente, \emph{necessário} em teorias de tipos. Por outro lado,
        relações tarskianas em geral se dão entre \emph{conjutos} de
        julgamentos.  Uma correção seria transformar as hipóteses de sistemas
        dedutivos em conjuntos, mas como comentado às vezes isso não é uma
        opção.

        Então qual é a relação entre essas duas classes de sistemas? De um lado,
        há relações tarskianas $\succ \subseteq \mathcal{P}\mathcal{J} \times
        \mathcal{J}$, e do outro sistemas dedutivo hipotéticos, que especificam
        provas de julgamentos da forma $\Gamma \vdash J$.

        Poderiamos esperar definir ``traduções'' de uma classe à outra. Como o
        tipo base dos julgamentos é o mesmo, a tradução entre coisas do tipo
        $\Delta \succ J$, onde $\Delta \subset \mathcal{J}$, e $\Gamma \vdash
        J$, onde $\Gamma$ é uma lista finita em $\mathcal{J}$ se resume a
        traduções entre conjuntos e listas finitas de forma coerente.

        A maneira que parece mais natural de fazer isso é tomar a
        \strong{imagem} da lista, para formar um conjunto, e pensar em listas
        finitas formadas de um conjunto, do outro lado. Ou seja

        \note{Trocar sistemas dedutivos aqui por relações tarskianas sobre
        listas}

        \begin{definition}
            A relação tarskiana $\succ_\vdash$ associada a um sistema dedutivo
            $\vdash$ (abusando a notação) é dada por

            \[
                \Delta \succ J \impliedby \text{existe $\Delta$-sequência}\;\Gamma
                \mathrel.\; \text{com}\; \Gamma \vdash J
            \]

            O sistema dedutivo $\vdash_\succ$ associado a uma relação tarskiana
            $\succ$ é dado por

            \[
                \Gamma \vdash J \impliedby \img\Gamma \succ J
            \]

        \end{definition}

        \note{Análise das traduções; teorema sobre adjunção}

    \section{Simetria}

        Depois de definir os tais dos sistemas dedutivos hipotéticos, podemos
        buscar especificar ainda mais os seus julgamentos. Imaginam-se, é claro,
        as suas contra-partes no mundo das relações tarskianas. Poderiamos
        imaginar algo do tipo

        \[
            \xymatrix{
                \text{Sistemas Dedutivos} \ar[d] \ar@{<.>}[r] & ? \ar@{.>}[d]\\
                \text{Sistemas Dedutivos Hipotéticos} \ar[d] \ar@{<=>}[r] & \text{Relações Tarskianas} \ar[d] \\
                \cdots & \cdots
            }
        \]

        É notável a falta de simetria. Somente esse fato já justifica buscar uma
        definição do que preencheria o buraco. Essa noção, quando definida,
        cumpriria o papel da ``relação tarskiana'' para os sistemas dedutivos, a
        versão sem-provas. Afirmaria, para cada julgamento, sua validade ou não.
        Ou seja, é um predicado sobre os julgamentos. Há, claro, uma noção de
        morfismo de predicados: dado predicados $\pi$ e $\tau$ sobre tipos de
        julgamentos $\mathcal{J}$ e $\mathcal{I}$, um \strong{morfismo} é uma
        função $\function{f}{\mathcal{J}}{\mathcal{I}}$ tal que $\forall j :
        \mathcal{J} \mathrel. \pi j \implies \pi (f j)$.

        Mas encontramos um problema: essa noção de morfismo não induz, como
        esperado, a noção esperada de morfismo nas relações tarskianas, agora
        vistas como predicados sobre as inferências de um tipo. Nesse caso, os
        morfismo seriam funções de $\mathrm{Inference}\mathcal{J}$ em
        $\mathrm{Inferece}\mathcal{I}$, e não de $\mathcal{J}$ em $\mathcal{I}$.

        A maneira de corrigir isso é considerar que predicados estão definidos
        não sobre um tipo, mas sobre a imagem de um tipo por um funtor. Assim,
        os morfismo entre predicados $\pi$ e $\tau$ sobre um funtor $F$ e
        objetos $\mathcal{J}$ e $\mathcal{I}$ são funções de $\mathcal{J}$ em
        $\mathcal{I}$ tal que $\forall \Gamma : F\mathcal{J} \mathrel. \pi
        \Gamma \implies \tau Ff\Gamma$

    \section{Enunciado}

    Tendo identificado conceitos que podem servir como base para uma
    generalização da Correspondência de Curry-Howard, basta identificarmos
    potenciais candidatos a enunciados do teorema nesse contexto abstrato, e
    analisá-los.

    A primeira observação mais relevante é que uma estrutura comum veio surgindo
    na nossa exploração: a de \strong{retração}, ou até \strong{retrato por
    deformação}. Genericamente, são enunciados da forma

    \begin{description}

        \item[Retração] A 1-flecha $\function r a b$ é tal que há 1-flecha $\function
            i b a$ tal que $f \circ i = \id b$ e
        
        \item[Retrato por deformação] há 2-flecha $\function \alpha {i \circ r}
            \id a$.

    \end{description}

    Numa linguagem mais orientada a elementos,

    \begin{description}
        \item[Retração] A função $\function r a b$ é tal que há função
            $\function i b a$ tal que $\forall y : b \mathrel. r\;(i\;y) = y$ e
            
        \item[Retrato por deformação] $\forall x : a \mathrel. i\;(r\;x) \sim x$
    \end{description}

    \note{Mencionar casos de retrações observadas}

    Chegamos à primeira formulação de uma possível correspondência de
    Curry-Howard:

    \begin{definition*}
        Dois sistemas dedutivos $\vdash$ e $\vdash'$ estão em
        \strong{Correspondência de Curry-Howard} se existe retração (retrato por
        deformação) de $\vdash$ a $\vdash'$.
    \end{definition*}

    É evidente, pensando no que já sabemos da \cch, que esperemos que isso seja
    verdade no caso mais clássico. No entanto, a definição parece um tanto
    vazia. É concebível que existam correspondências do tipo que não gostariamos
    de chamar de ``Correspondência de Curry-Howard''. Idealmente, gostariamos de
    poder dizer que os sistemas estão em \cch\ quando há retração (retrato por
    deformação) entre os dois \emph{e} quando um é ``uma lógica'' e o outro uma
    ``teoria de tipos''.

\end{document}
