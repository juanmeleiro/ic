module ProoffulDeductiveSystems

import HypotheticalDeductiveSystems

public export
data TypingJudgement : (terms : Type) -> (types : Type) -> Type where
  HasType : terms -> types -> TypingJudgement terms types
  IsType : types -> TypingJudgement terms types

public export
ProoffulRuleset : Type -> Type -> Type
ProoffulRuleset terms types = HypotheticalRuleset (TypingJudgement terms types)
