module HypotheticalDeductiveSystems

import Data.Vect
import DeductiveSystems
import Utilities

public export
-- @ Context
Context : Type -> Type
Context p = (n : Nat ** Vect n p)
-- @

public export
-- @ HypotheticalRuleset
HypotheticalRuleset : Type -> Type
HypotheticalRuleset j = Ruleset (Inference j)
-- @


public export
-- @ len
len : {J : Type} ->
      Context J ->
      Nat
len (n ** _) = n
-- @

public export
-- @ con
body : {J : Type} ->
       (h : Context J) ->
       Vect (len h) J
body (_ ** ctx) = ctx
-- @

-- @ empty
empty : {A : Type} -> Context A
empty = (0 ** Nil)
-- @
