module PropositionalDeductiveSystems

import Data.Vect
import HypotheticalDeductiveSystems
import AffirmativeDeductiveSystems
import DeductiveSystems
import Utilities

-- Positive headers are the fundamental data that defines a primitive
-- introduction rule.
PositiveHeader : Nat -> Type
PositiveHeader n = Vect n LöfJudgement
-- PositiveHeader = Context Löf

-- Negative headers are the fundamental data that defines a primitive
-- elimination rule.
NegativeHeader : Type
NegativeHeader = ?NegativeHeader

-- A positive connective is a collection of positive headers with the same
-- arity.
PositiveConnective : Type
PositiveConnective = (n : Nat ** (Subset (PositiveHeader n)))

-- A negative connective is a colletion of negative headers with the same
-- arity.
NegativeConnective : Type

-- Given a collection of positive and negative connectives, we define what are
-- the *propositions* over them. This is not, I find, strictly in the spirit of
-- what Martin Löf argues, for there he takes propositions as things we judge as
-- propositions, and not explicitly variables and connectives combined
-- inductively. Still, here a proposition is either a propositional variable or
-- a sequence of propositions combined using a connective. Here the arity takes
-- meaning.
data PropositionOverHeaders : Subset PositiveConnective -> Subset NegativeConnective -> Type where
  PropositionalVariable : Nat -> PropositionOverHeaders p n
  FromPositive : {p : Subset PositiveConnective} ->
                 {n : Subset NegativeConnective} ->
                 (k : PositiveConnective) ->
                 p k ->
                 Vect (fst k) (PropositionOverHeaders p n) ->
                 PropositionOverHeaders p n
  -- FromNegative : {p : Subset PositiveConnective} ->
  --                {n : Subset NegativeConnective} ->
  --                (k : NegativeConnective) ->
  --                n k ->
  --                Vect (fst k) (PropositionOverHeaders p n) ->
  --                PropositionOverHeaders p n

-- The propositional axioms are that propostional variables are propositions.
-- That is, given a collection of connectives and a natural number (identifying
-- a variable), we may conclude that that variable is a proposition.                                                                  
PropositionalAxiom : (p : Subset PositiveConnective) ->
                     (n : Subset NegativeConnective) ->
                     Nat ->
                     Inference (Inference (Löf (PropositionOverHeaders p n)))
PropositionalAxiom p n k = ?axiom

-- A positive inference is a rule that allows us to conclude a proposition
-- formed with a connective is true given the premises specified in one of its
-- headers.
PositiveInference : {p : Subset PositiveConnective} ->
                    {n : Subset NegativeConnective} ->
                    (k : PositiveConnective) ->
                    (h : PositiveHeader (fst k)) ->
                    p k ->
                    (snd k) h ->
                    Context (PropositionOverHeaders p n) ->
                    Vect (fst k) (PropositionOverHeaders p n) ->
                    Inference (Inference (Löf (PropositionOverHeaders p n)))
PositiveInference k h k_in_p h_in_k ctx props = ((fst k ** hyp), con) where
  judg = zip props h
  hyp = map (\ j => (ctx, j)) judg
  con = (ctx, (FromPositive k k_in_p props) true)

PositiveElimination : {p : Subset PositiveConective} ->
                      {n : Subset NegativeConnective} ->
                      (k : PositiveConnective) ->
                      p k ->
                      Context (PropositionOverHeaders p n) ->
                      Vect (fst k) (PropositionOverHeaders p n) ->
                      PropositionOverHeaders p n ->
                      Inference (Inference (Löf (PropositionOverHeaders p n)))
PositiveElimination k k_in_p ctx props con = ?poselim

-- A negative inference is a rule that allows us to extract a judgement from the
-- judgement that a proposition formed from a connective is true.
NegativeInference : Type

-- A propositional ruleset is defined from a collection of connectives. It's a
-- ruleset over the propositions formed from those connectives, admiting the
-- rules given by the headers of those connectives.
PropositionalRuleset : (p : Subset PositiveConnective) ->
                       (n : Subset NegativeConnective) ->
                       AffirmativeRuleset (PropositionOverHeaders p n)
PropositionalRuleset = ?propruleset
