||| Intuitionistic Propositional Logic
module IPL

import DeductiveSystems
import AffirmativeDeductiveSystems
import Data.Vect
import Utilities

Signature : Type -> Type
Signature l = l -> Nat

data Proposition : Signature l -> Type -> Type where
  Atomic : v -> Proposition _ v
  Apply : {s : Signature l} ->
          (c : l) ->
          Vect (s c) (Proposition s v) ->
          Proposition s v

-- Now, we want to define the data needed to specify a propositional ruleset
-- over a specific signature. That data must include a specification of what
-- labels are positive and which are negative, and must also provide the
-- introduction or elimination rules accordingly.

data Polarity = Positive | Negative

Polarization : Type -> Type
Polarization l = l -> Polarity

data LofAdjective : Type where
  IsTrue : LofAdjective
  IsProp : LofAdjective

PolarHeader : (s : Signature l) -> (p : Polarization l) -> l -> Type
PolarHeader s p c = case p c of
                         Positive => Vect (s c) LofAdjective
                         Negative => ?something_else
  -- PositiveHeader : (c : l) -> Vect (s c) LofAdjective -> PolarHeader s Positive
  -- NegativeHeader :  -> PolarHeader s Negative

Specification : (s : Signature l) -> (p : Polarization l) -> Type
Specification {l} s p = (c : l) -> Subset (PolarHeader s p c)

PropositionalRuleset : Specification {l} s p -> Ruleset (Inference (Löf (Proposition s l)))

data StandardConnectives = And | Or | Imply | Bottom

StandardSignature : Signature StandardConnectives
StandardSignature c = case c of
                           And => 2
                           Or => 2
                           Imply => 2
                           Bottom => 0

StandardPolarization : Polarization StandardConnectives
StandardPolarization c = case c of
                          And => Negative
                          Or => Positive
                          Imply => Negative
                          Bottom => Positive

OrHeader : Subset (PolarHeader StandardSignature StandardPolarization Or)
OrHeader l = case l of
                  [IsTrue, IsProp] => ()
                  [IsProp, IsTrue] => ()
                  _ => Void

StandardSpecification : Specification StandardSignature StandardPolarization
StandardSpecification c = case c of
                            And => ?and_header
                            Or => OrHeader
                            Imply => ?imply_header
                            Bottom => empty


