module AffirmativeDeductiveSystems

import DeductiveSystems
import HypotheticalDeductiveSystems
import Data.Vect
import Data.Fin
import Utilities

-- @ Atomic
public export
Atomic : Type -> Type -> Type
Atomic = Pair
-- @

-- @ LöfJudgement
public export
data LöfJudgement : Type where
  IsTrue : LöfJudgement
  IsProp : LöfJudgement
-- @

-- @ Löf
public export 
Löf : Type -> Type
Löf p = Atomic p LöfJudgement
-- @

-- @ Löf-syntax
syntax [p] true = (p, IsTrue)
syntax [p] prop = (p, IsProp)
-- @

public export
-- @ AffirmativeDeductiveRuleset
AffirmativeRuleset : Type -> Type
AffirmativeRuleset p = HypotheticalRuleset (Löf p)
-- @

-- Common AffirmativeDeductiveSystem's rules
self : {P : Type} ->
       Context (Löf P) ->
       P ->
       Inference (Inference (Löf P))
self (n ** ctx) p = ((1 ** hyp), con) where
  hyp = is_prop :: Nil where
    is_prop = ((n ** ctx), p prop)
  con = ((S n ** p true :: ctx), p true)

weak : {P : Type} ->
       Context (Löf P) ->
       Löf P ->
       P ->
       Inference (Inference (Löf P))
weak (n ** ctx) j p = ((2 ** hyp), con) where
  hyp = org :: wff :: Nil where
    org = ((n ** ctx), j)
    wff = ((n ** ctx), p prop)
  con = ((S n ** p true :: ctx), j)

-- The two common rules aggregated into a Ruleset
Minimal : {P : Type} -> Inference (Inference (Löf P)) -> Type
Minimal {P} r = Either (ctx : Context (Löf P) ** (A : P ** r = self ctx A))
                   (ctx : Context (Löf P) ** (j : Löf P ** (p : P ** r = weak ctx j p)))

-- This is wrong. The theorem is true if we only allow for `p true` judgements
                                         -- on the context. That's because
                                         -- we only have weakening for putting
                                         -- truth judgements into context. So
                                         -- that must be specified.
                                         -- 
                                         -- This is kept only for reference for
                                         -- when its fixed.
-- reflexivity : {P : Type} ->
--               (h : Context (Löf P)) ->
--               ((i : Fin (len h)) ->
--                   Deduction Minimal (empty, (fst (index i (con h))) prop)) ->
--               (k : Fin (len h)) ->
--               Deduction Minimal (h, (fst (index k (con h))) true)
-- reflexivity (n ** ctx) wfh i = ?reflexivity
