module DeductiveSystems

import Data.Fin
import Data.Vect
import Utilities

%access public export

||| Represents a inferential step between judgements of type `j`
|||
||| An inferential step consists of a vector of elements of `j`, to be
||| interpretet as the hypothesis, and a single element of `j`, to be
||| interpreted as a conclusion.
-- @ Inference
Inference : Type -> Type
Inference j = ((n : Nat ** Vect n j), j)
-- @

-- @ Inference-utilities
arity : {J : Type} -> Inference J -> Nat
arity ((n ** _), _) = n
-- @

-- @ Inference-utilities
hypothesis : {J : Type} -> (R : Inference J) -> Vect (arity R) J
hypothesis ((_ ** h), _) = h
-- @

-- @ Inference-utilities
conclusion : {J : Type} -> Inference J -> J
conclusion ((_ ** _), c) = c
-- @

||| Represents allowable inferences in some particular deductive system
|||
||| A collection of steps, or `Inference`s, that are allowed to be used in a
||| deduction.
-- @ Ruleset
Ruleset : Type -> Type
Ruleset j = Subset (Inference j)
-- @

||| The type of deductions of judgements in `J` using inferences allowed by a
||| `Ruleset`
|||
||| `Deduction` defines inductively the possible deductions, using inferences
||| in some particular `Ruleset`, of some particular judgement in `J`.
-- @ Deduction
data Deduction : {J : Type} -> Ruleset J -> J -> Type where

  ||| `Infer` a judgement from some hypothesis given an `Inference`
  |||
  ||| A deduction of a judgement is a particular inference -- with that
  ||| judgement as conclusion -- applied to deductions of all the hypothesis.
  ||| @ J The type of judgements
  ||| @ R The ruleset of allowable inferences
  ||| @ r The particular inference to be applied
  ||| @ allowed A proof that that inference is allowed
  ||| @ deductions Deductions of the hypothesis of `r`
  Infer : {J : Type} ->                           -- Given a base type J,
          {R : Ruleset J} ->                      -- a ruleset R,
          {r : Inference J} ->                    -- and an inference r,
          (allowed : R r) ->                      -- if r is admited by R,
          (deductions : (i : Fin (arity r)) ->    -- and we have a proof of its hypothesis,
            (Deduction R (index i (hypothesis r)))) ->
          Deduction R (conclusion r)              -- we get a proof of the conclusion
-- @

||| A coherent translation between deductions on two rulesets
record RulesetMorphism j k (r : Ruleset j) (s : Ruleset k) where
  constructor MkRulesetMorphism
  translate : j -> k
  transport : {x : j} -> Deduction r x -> Deduction s (translate x)
