module PredicatesAndDeductiveSystems

import Predicates.Predicates
-- import Predicates.PredicateCategory
import DeductiveSystems.DeductiveSystems
import Data.Vect

irrelevant : Ruleset j -> Predicate j
irrelevant r c = Deduction r c

relevant : Predicate j -> Ruleset j
relevant p inference = Pair (arity inference = 0) (p (conclusion inference))

-- Have to be cleaned-up. The extraneous argument x1 is weird: in `nevermind`,
-- it seems to be applied without being in scope.
untangle : (p : Predicate j) -> (x1 : j) -> irrelevant (relevant p) x1 -> p (id x1)
untangle p (conclusion r) (Infer allowed deductions) = snd allowed

nevermind : (p : Predicate j) -> PredicateMorphism j j (irrelevant (relevant p)) p
nevermind p = MkPredicateMorphism id (untangle p x1)

tangle : (p : Predicate j) -> (x1 : j) -> p x1 -> irrelevant (relevant p) (id x1)
tangle p x1 pr = Infer {r=((0 ** Nil), x1)} (Refl, pr) (\ i => FinZElim i)

overmind : (p : Predicate j) -> PredicateMorphism j j p (irrelevant (relevant p))
overmind p = MkPredicateMorphism id (tangle p x1)

-- isomind : (p : Predicate j) -> (nevermind p) . (overmind p) = id

-- The same remark applies here as it applies to `untangle`
unwind : Deduction (relevant (irrelevant r)) x1 -> Deduction r (id x1)
unwind (Infer allowed deductions) = snd allowed

underthink : (r : Ruleset j) -> RulesetMorphism j j (relevant (irrelevant r)) r
underthink r = MkRulesetMorphism id unwind

wind : Deduction r x1 -> Deduction (relevant (irrelevant r)) (id x1)
wind (Infer _ _) = ?wind_rhs

overthink : (r : Ruleset j) -> RulesetMorphism j j r (relevant (irrelevant r))
overthink r = MkRulesetMorphism id wind
