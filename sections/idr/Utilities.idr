module Utilities

import Data.Fin
import Data.Vect

public export
Subset : Type -> Type
Subset a = a -> Type

-- syntax [x] "in" [y] = y x

public export
injective : {a, b : Type} ->
            (a -> b) ->
            Type
injective {a} {b} f = (x, y : a) ->
                      f x = f y ->
                            x = y

public export
surjectiveOver : {a : Type} ->
                 {b : Type} ->
                 (p : Subset b) ->
                 (a -> b) ->
                 Type
surjectiveOver {a} {b} p f = (y : b) ->
                             (p y) ->
                             (x : a ** f x = y)

public export
subset : {a : Type} ->
         Subset a ->
         Subset a ->
         Type
subset {a} p q = (x : a) -> (p x) -> (q x)

public export
finite : {a : Type} ->
         Subset a ->
         Type
finite {a} p = (n : Nat ** (f : (Fin n -> a) ** (injective f, surjectiveOver p f)))

public export
image : Vect n a -> Subset a
image {n} v x = (n : Fin n ** index n v = x)

public export
empty : Subset a
empty = const Void
