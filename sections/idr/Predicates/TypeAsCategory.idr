module TypeAsCategory

import Basic.Category

%access public export

||| The functions from a type to another
Functions : Type -> Type -> Type
Functions a b = (a -> b)

||| The identity function for a type
Id : (a : Type) -> Functions a a
Id a = id

||| The usual composition
Compose : (a, b, c : Type) -> Functions a b -> Functions b c -> Functions a c
Compose a b c = flip (.)

||| Left function composition with the identity gives the same function
|||
||| The proof becomes evident by evaluating the terms:
|||
||| ```
||| Compose a a b (Id a) f
||| = flip (.) (Id a) f    -- by definition of Compose
||| = f . (Id a)           -- by definition of flip
||| = f . id               -- by definition of Id
||| = \x => f (id x)       -- by definition of (.)
||| = \x => f x            -- by definition of id
||| ```
|||
||| Thus, the equality reduces to the eta-conversion axiom.
LeftIdentity : (a, b : Type) -> (f : Functions a b) -> Compose a a b (Id a) f = f
LeftIdentity a b f = Refl

||| Right function composition with the identity gives the same function
|||
||| See documentation on `LeftIdentity`.
RightIdentity : (a, b : Type) -> (f : Functions a b) -> Compose a b b f (Id b) = f
RightIdentity a b f = Refl

||| Function composition is associative
|||
||| Similar to `LeftIdentity`, we find the proof by expanding definitions.
|||
||| First, the left side:
||| ```
||| Compose a b d f (Compose b c d g h)
||| = flip (.) f (Compose b c d g h) -- by definition of `Compose`
||| = (Compose b c d g h) . f        -- by definition of `flip`
||| = (flip (.) g h) . f             -- by definition of `Compose`
||| = (h . g) . f                    -- by definition of `flip`
||| = \x => (h . g) (f x)            -- by definition of (.)
||| = \x => (\y => h (g y)) (f x)    -- by definition of (.)
||| = \x => h (g (f x))              -- by beta-conversion
||| ```
|||
||| Then, the right:
|||
||| ```
||| Compose a c d (Compose a b c f g) h
||| = flip (.) (Compose a b c f g) h -- by definition of `Compose`
||| = h . (Compose a b c f g)        -- by definition of `flip`
||| = h . (flip (.) f g)             -- by definition of `Compose`
||| = h . (g . f)                    -- by definition of `flip`
||| = \x => h ((g . f) x)            -- by definition of `(.)`
||| = \x => h ((\y => g (f y)) x)    -- by definition of `(.)`
||| = \x => h (g (f x))              -- by beta-conversion
||| ```
|||
||| Thus, the two terms are syntatically the same.
Associativity : (a, b, c, d : Type) ->
                (f : Functions a b) ->
                (g : Functions b c) ->
                (h : Functions c d) ->
                Compose a b d f (Compose b c d g h) = Compose a c d (Compose a b c f g) h
Associativity a b c d f g h = Refl


||| The Category of Types
||| 
||| The category formed by the types with functions as arrows.
TypeCat : Category                 -- The category formed
TypeCat = MkCategory Type          -- by the types 
                     Functions     -- with functions as arrows,
                     Id            -- the id function as identity,
                     Compose       -- usual composition as composition,
                     LeftIdentity  -- which follow the left identity,
                     RightIdentity -- right identity,
                     Associativity -- and associativity laws.
