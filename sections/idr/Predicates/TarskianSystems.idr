module TarskianSystems

import Data.Fin
import Utilities

public export
-- @ TarskianRelation
TarskianRelation : Type -> Type
TarskianRelation j = Subset (Subset j, j)
-- @

public export
-- @ Tarskian
Tarskian : Type
Tarskian = (j : Type ** TarskianRelation j)
-- @

ClosureOperator : Type -> Type
ClosureOperator j = Subset j -> Subset j

expanding : ClosureOperator j -> Type
expanding {j} f = (ax : Subset j) ->
                  subset ax (f ax)

increasing : ClosureOperator j -> Type
increasing {j} f = (ax, ax' : Subset j) ->
                   subset ax ax' ->
                   subset (f ax) (f ax')

infixr 8 ~~ -- Totally ad-hoc, trial-and-error

(~~) : (a -> b) -> (a -> b) -> Type
(~~) {a} f g = (x : a) -> f x = g x

-- postulate extensionality : f ~~ g -> f = g

idempotent : (a -> a) -> Type
idempotent f = f . f ~~ f


