||| Definition of the `Category` of `Predicate`s
module Predicates

-- import Utilities
-- import Functorial
import TypeAsCategory
import Basic.Category
import Basic.Functor
import PointedFunctors

%access public export

-- # The constructions

||| A `Subset` of an image of a `Type` under a `PointedFunctor`
|||
||| A predicate assings to a judgement of a format `f`, which is a pointed
||| functor, over a base type `a` to the type of its proofs.
Predicate : Type -> PointedFunctor TypeCat -> Type
Predicate a f = (mapObj (functor f) a) -> Type

apply : PointedNaturalTransformation TypeCat f g ->
        (a -> b) ->
        mapObj (functor f) a -> mapObj (functor g) b

||| The arrows of the Predicate category
|||
||| A `PredicateMorphism` between two predicates `p` and `q` is given by a
||| function on its base types that is coherent with the proofs when lifted by
||| the funtors.
record PredicateMorphism (a : Type)
                         (b : Type)
                         (f : PointedFunctor TypeCat)
                         (g : PointedFunctor TypeCat)
                         (p : Predicate a f)
                         (q : Predicate b g) where
  constructor MkPredicateMorphism
  Translate : a -> b
  Transform : PointedNaturalTransformation TypeCat f g
  Transport : {x : mapObj (functor f) a} -> p x -> q (apply Transform Translate x)

IdentityPredicateMorphism : (p : Predicate a f) -> PredicateMorphism a a f f p p
IdentityPredicateMorphism p = MkPredicateMorphism id idnat idtrans where
  idnat = ?idnat
  idtrans = ?idtrans

ComposePredicateMorphism : PredicateMorphism a b f g p q ->
                           PredicateMorphism b c g h q r ->
                           PredicateMorphism a c f h p r
ComposePredicateMorphism phi psi = MkPredicateMorphism translate transform transport where
  translate = translate psi . translate phi
  transform = ?tl -- transport psi (transport phi pi)
  transport = ?tp

-- # The laws
                                                  
LeftPredicateIdentity : (phi : PredicateMorphism a b f g p q) ->
                        ComposePredicateMorphism (IdentityPredicateMorphism p) phi = phi
RightPredicateIdentity : (phi : PredicateMorphism a b f g p q) ->
                         ComposePredicateMorphism phi (IdentityPredicateMorphism q) = phi
Associativity : (phi : PredicateMorphism a b f g p q) ->
                (psi : PredicateMorphism b c g h q r) ->
                (chi : PredicateMorphism c d h i r s) ->
                ComposePredicateMorphism phi (ComposePredicateMorphism psi chi) =
                  ComposePredicateMorphism (ComposePredicateMorphism phi psi) chi

-- # The category

PredCat : Category

-- Proofs : Predicate j -> Type
-- Proofs {j} proofs = (phi : j ** proofs phi)

-- conclusion : (r : Predicate j) -> Proofs r -> j
-- conclusion r (phi ** pi) = phi
-- 
-- public export
-- internalize : (proofs : Predicate j) -> Predicate (Proofs proofs, j)
-- internalize proofs (pi, phi) = (conclusion proofs pi = phi)
-- 
-- public export
-- injectiveness : (proofs : Predicate j) -> injective (conclusion (internalize proofs))
-- injectiveness proofs alpha = ?inj
-- 
-- public export
-- indepotence : (proofs : Predicate j) -> internalize (internalize proofs) = internalize proofs
