module PointedFunctors

import Basic.Category
import Basic.Functor
import Basic.NaturalTransformation

%access public export

record PointedFunctor (c : Category) where
  constructor MkPointedFunctor
  functor : CFunctor c c
  punctuation : {a : obj c} -> mor c a (mapObj functor a)

record PointedNaturalTransformation (c : Category)
                                    (f : PointedFunctor c)
                                    (g : PointedFunctor c) where
  constructor MkPointedNaturalTransformation
  transformation : NaturalTransformation c c (functor f) (functor g)
  -- punctuated : ?look_at_diagram

