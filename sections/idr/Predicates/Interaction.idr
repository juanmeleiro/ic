module Interaction

import Utilities
import TarskianSystems
import DeductiveSystems
import HypotheticalDeductiveSystems
import Data.Fin
import Data.Vect

public export
finitary : TarskianRelation j -> Type
finitary {j} r  = (hyp : Subset j) ->
              (con : j) ->
              r (hyp, con) ->
              (hyp' : Subset j **
                (finite hyp', subset hyp' hyp, r (hyp', con)))

public export
FinitaryTarskianRelation : Type -> Type
FinitaryTarskianRelation j = (r : TarskianRelation j ** finitary r)

-- Commutative Rulesets and Permutable Hypothetical Rulesets

public export
-- @ IsPermutation
IsPermutation : {J : Type} ->
                Context J ->
                Context J ->
                Type
IsPermutation (n ** ctx)
              (m ** ctx') =
                (n = m,
                (f : (Fin n -> Fin m) **
                      (injective f,
                        (i : Fin n) -> index i ctx = index (f i) ctx')))
-- @

commutative : Ruleset j -> Type
commutative {j} r = (ctx : Context j) ->
                    (ctx' : Context j) ->
                    (con : j) ->
                    IsPermutation ctx ctx' ->
                    r (ctx, con) ->
                    r (ctx', con)
CommutativeRuleset : Type -> Type
CommutativeRuleset j = (r : Ruleset j ** commutative r)

public export
-- @ permutable
permutable : HypotheticalRuleset j -> Type
permutable {j} r = (ctx : Context j) ->
                   (con : j) ->
                   (Deduction r (ctx, con)) ->
                   (ctx' : Context j) ->
                   IsPermutation ctx ctx' ->
                   Deduction r (ctx', con)
-- @

public export
-- @ PermutableHypotheticalRuleset
PermutableHypotheticalRuleset : Type -> Type
PermutableHypotheticalRuleset j = (r : HypotheticalRuleset j ** permutable r)
-- @

transport : {a, b : Type} -> (x : a) -> a = b -> b
transport x mu = case mu of
                      Refl => x

permutationHasSameImage : {J : Type} ->
                          (ctx : Context J) ->
                          (ctx' : Context J) ->
                          IsPermutation ctx ctx' ->
                          image (body ctx) = image (body ctx')
permutationHasSameImage = ?perm

vectorify : TarskianRelation j -> CommutativeRuleset j
vectorify {j} r = (r' ** com) where
  r' : Ruleset j
  r' ((n ** hyp), con) = r ((image hyp), con)
  com : commutative r'
  com hyp hyp' con is_perm is_con = ?com
-- Argument for com:
-- r' (hyp', con) = r (image hyp', con) by definition
--                = r (image hyp, con)  because image hyp' = image hyp because is_perm
--                = r' (hyp, con)       by definition
-- And is_con : r' (hyp, con)

Sequence : Subset a -> Type
Sequence {a} x = (ctx : Context a ** subset (image (body ctx)) x)
-- Sequence {a} x = (n : Nat ** (s : Vect n a ** subset (image s) x))

setify : Ruleset j -> FinitaryTarskianRelation j
setify {j} r = (r' ** fin) where
  r' : TarskianRelation j
  r' (hyp, con) = (seq : Sequence hyp ** case seq of (hyp' ** is_seq) => r (hyp', con))
  fin : finitary r'
  fin hyp con is_con = case is_con of
                            (seq ** is_con') => (image (body (fst seq)) ** (?finite, snd seq, ?is_con))

-- Previous functions are inverses when restricted to FinitaryTarskianRelation
-- j and CommutativeRuleset j.
