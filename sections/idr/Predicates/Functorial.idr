module Functorial

import Utilities

-- Functor stuff

record Functor where
  constructor MkFunctor
  base : Type -> Type
  lift : {a, b : Type} -> (a -> b) -> (base a -> base b)

record NaturalTransformation (f : Functor) (g : Functor) where
  constructor MkNaturalTransformation
  arrow : (a : Type) -> base f a -> base g a
  coherence : (a, b : Type) ->
              (h : a -> b) ->
              (arrow b) . (lift f h) = (lift g h) . (arrow a) 


record PointedFunctor where
  constructor MkPointedFunctor
  funct : Functor
  point : (a : Type) -> (a -> base funct a)

record PointedFunctorMorphism (f : PointedFunctor) (g : PointedFunctor) where
  constructor MkPointedFunctorMorphism
  transf : NaturalTransformation (funct f) (funct g)
  coherence : ?well

elevate : PointedFunctorMorphism f g -> (a -> b) -> base (funct f) a -> base (funct g) b
elevate mu f x = ?elevate

-- Deductive Relation Stuff

DeductiveRelation : Functor -> Type -> Type
DeductiveRelation f a = Subset (base f a, a)

record DeductiveRelationMorphism (f : Functor)
                                 (a: Type)
                                 (r : DeductiveRelation f a)
                                 (b : Type)
                                 (r' : DeductiveRelation f b) where
  constructor MkDeductiveRelationMorphism
  transfer : a -> b
  coherence : (hyp : base f a) ->
              (con : a) ->
              r (hyp, con) -> r' (lift f transfer hyp, transfer con)

record PointedDeductiveRelationMorphism (f : PointedFunctor)
                                        (a : Type)
                                        (r : DeductiveRelation (funct f) a)
                                        (g : PointedFunctor)
                                        (b : Type)
                                        (s : DeductiveRelation (funct g) b) where
  constructor MkPointedDeductiveRelationMorphism
  transfer : a -> b
  translate : PointedFunctorMorphism f g
  coherence : ?coherence



