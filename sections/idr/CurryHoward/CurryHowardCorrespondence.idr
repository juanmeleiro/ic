module CurryHowardCorrespondence

import AffirmativeDeductiveSystems
import ProoffulDeductiveSystems

CurryHoward : AffirmativeRuleset j ->
              ProoffulRuleset t j ->
              Type
CurryHoward pl tt = ?curryhoward
