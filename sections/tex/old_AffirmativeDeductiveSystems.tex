\documentclass[../main.tex]{subfiles}

\begin{document}
    
    \section{Sistemas dedutivos afirmativos}

    Especificando os sistemas dedutivos, obtemos uma subclasse que denominamos
    os \strong{sistemas dedutivos naturais}, ou aqueles a que lhes concerne a
    noção de \emph{verdade}, e de \emph{inferência}, no sentido comum da
    palavra. Isto é, aqueles em que os julgamentos são contingentes em hipóteses
    -- da forma $\Gamma \vdash J$-- e cujas hipóteses (e conclusões) são da
    forma $P \true$ ou $P \prop$ -- indicando a verdade da proposição $P$, ou
    sua boa-formação.

    Então nosso objetivo, no momento, é especificar o tipo $\mathcal{J} : \Type$
    que usamos anteriormente. Chamemos os tais julgamentos que são elementos do
    contexto, ou que são conclusões, de julgamentos atômicos. Nossa intenção,
    neste caso, é tomar os julgamentos atômicos como uma espécie de união
    disjunta de um tipo das proposições $P$ com ele mesmo. Poderiamos definir
    como $P + P$, que é naturalmente interpretado como 
    
    \[
        \sum_{i : 2} P_i
    \]

    onde $P_0 = P$ e $P_1 = P$. Claro, podemos emular isso com uma notação mais
    simpática, com dois construtores em notação posfixa

    \begin{align*}
        \mathsf{true} &: P \rightarrow \Atomic P \\
        \mathsf{prop} &: P \rightarrow \Atomic P
    \end{align*}

    O problema dessa abordagem é que ela torna difícil genralizar para
    variedades mais exóticas de julgamentos, além de $\true$ e $\prop$. Para
    isso, o ideal é que essa classe de julgamentos atômicos seja um parâmetro do
    nosso tipo. Então podemos definir da seguinte forma:

    \begin{align*}
        \Atomic& : \Type \rightarrow \Type \rightarrow \Type \\
        \Atomic& = \times
    \end{align*}

    E em seguida instanciamos $\Atomic\;P\;\Lof$ onde

    \begin{align*}
         \Lof &: \Type \\
        \true &: \Lof \\
        \prop &: \Lof \\[2\baselineskip]
    \end{align*}

    É claro, denotaremos $(\alpha, {\true})$ e $(\alpha, {\prop})$ por
    $\alpha\true$ e $\alpha\prop$, respectivamente.

    Agora, o tipo dos julgamentos $\mathcal{J}$ com que estavamos trabalhando
    será o tipo das deduções dos julgamentos atômicos em julgamentos atômicos.

    \[
        \Inference \; (\Atomic \; P \; \Lof)
    \]

    cujos elementos $(n, (\Gamma, J))$ denotaremos por $\Gamma \vdash J$. A
    aridade não só em geral é clara pelo contexto, também pode ser recuperada
    como $\mathop{len}\Gamma$, ou ainda com o extrator $\arity$ que definimos
    anteriormente.

    \subsection{Algumas regras comuns}

    Duas regras, em particular, são comuns e talvez até necessárias para o
    funcionamento de um sistema dedutivo afirmativo. São o enfraquecimento e a
    reflexividade. Simbolicamente,

    \begin{mathpar}
        \infer {
            \Gamma \vdash P \prop
        } {
            \Gamma, P \true \vdash P \true
        } \and
        \infer {
            \Gamma \vdash J \\
            \Gamma \vdash P \prop
        } {
            \Gamma, P \true \vdash J
        }
    \end{mathpar}

    Formalmente,

    \begin{align*}
        \self& : (P : \Type) \rightarrow
                (\Gamma : \Context P) \rightarrow
                (A : P) \rightarrow
                \Inference\;(\Inference\;(\Atomic \; P \; \Lof)) \\
        \self& \; P \; \Gamma \; A = (1,
                                      \const \; (\pi_L\Gamma,
                                                 \pi_R\Gamma,
                                                 A \prop),
                                      (\suc\pi_L\Gamma,
                                       (\pi_R\Gamma,
                                        A \true),
                                      A \true)) \\[2\baselineskip]
        \weak& : (P : \Type) \rightarrow
                 (\Gamma : \Context P) \rightarrow
                 (J : \Atomic P \Lof) \rightarrow
                 (A : P) \rightarrow
                 \Inference (\Inference (\Atomic P \Lof))\\
        \weak&_P\;\Gamma\;J\;A = (
                                2, (
                                    [
                                        (
                                            \pi_L\Gamma,
                                            (
                                                \pi_R\Gamma,
                                                J
                                            )
                                        ), (
                                            \pi_L\Gamma,
                                            (
                                                \pi_R\Gamma,
                                                (
                                                    P,
                                                    {\prop}
                                                )
                                            )
                                        )
                                    ]
                                ), (
                                    (
                                        \suc\pi_L\Gamma,
                                        \pi_R\Gamma :: (
                                            A, 
                                            {\true}
                                            ),
                                        J
                                    )
                                )
                              ) % Conclusion
    \end{align*}

    Podemos colecionar essas regras (perceba que $\self$ e $\weak$ são
    \emph{esquemas} de regras) usando um predicado sobre o tipo das regras.

    \begin{align*}
        \mathrm{Minimal}& : (P : \Type) \rightarrow
                            \Set (\Inference (\Inference (\Atomic P;\Lof))) \\
        \mathrm{Minimal}&_P R = \left(
            \sum_{\Gamma : \Context P}
            \sum_{A : P}
            R = \self_P\;\Gamma\;A
        \right) \times \left(
            \sum_{\Gamma : \Context P}
            \sum_{J : \Atomic P\;\Lof}
            \sum_{A : P}
            R = \weak_P \Gamma\;J\;A
        \right)
    \end{align*}

    \subsection{Alguns resultados}

    Agora podemos provar alguns resultados sobre estes sistemas. Por exemplo, a
    reflexividade forte.

    \begin{proposition}
        Num sistema dedutivo que conte com as regras $\self$ e $\weak$, vale a
        reflexividade forte. Isto é, dado contexto válido $\Gamma$ e índice $i :
        \Fin_{\pi_L\Gamma}$, há uma prova de $\Gamma \vdash \pi_R\Gamma\;i$.
        \[
            \mathrm{reflexivity} : \prod_{P : \Type}
                                   \Context P \rightarrow
                                   \prod_{i : \Fin_{\pi_L\Gamma}} \rightarrow
                                   \Deduction_{\Inference (\Atomic P \Lof)}
                                       \;\mathrm{Minimal}
                                       \;(\Gamma \vdash \pi_R\Gamma\;i)
        \]
        \begin{proof}
            Falta preencher.
        \end{proof}
    \end{proposition}


\end{document}
