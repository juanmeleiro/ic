\documentclass[../main.tex]{subfiles}

\begin{document}

    \section{Regulamentos}

    A primeira definição a ser feita é a dos \emph{regulamentos}. Como discutido
    antes, há dois tipos de regulamento -- duas maneiras de se definir um
    conectivo. São os regulamentos \strong{positivos} e \strong{negativos}.
    
    Para começar, definimos o que são regras de introdução e eliminação
    primitivas.

    \begin{definition}[Regra de introdução primitiva]
        \label{def:regra-de-introdução-primitiva}
        Uma \strong{regra de introdução primitiva} é definida por uma aridade $n
        : \mathbb{N}$ e um \strong{cabeçalho}, uma função de $n$ em
        $\mathbb{J}$. A regra com aridade $n$ e cabeçalho $\sigma$ é
        interpretada como a regra de inferência
        \[
            \inferrule {
                \Gamma \vdash A_0 \; \sigma(0) \\
                \cdots \\
                \Gamma \vdash A_{n-1} \; \sigma(n-1)
            } {
                \Gamma \vdash K(A_1, \cdots, A_{n-1})
            }
        \]

        O tipo das regras de introdução primitiva é, então,

        \begin{align*}
            &\mathrm{PrimitiveIntro} : \mathbb{N} \rightarrow \mathrm{Type} \\
            &\mathrm{PrimitiveIntro} n = \mathrm{Vec}_n \mathbb{J}
        \end{align*}
    \end{definition}

    \begin{definition}[Regulamento Positivo]
        \label{def:regulamentos-positivo}
        Um \strong{regulamento positivo} é uma sequencia de regras de introdução
        primitivas de mesma aridade. Ou seja,

        \begin{align*}
            &\mathrm{PositiveRuleset} : \mathrm{Type} \\
            &\mathrm{PositiveRuleset} = \sum_{n,k : \mathbb{N}} \mathrm{Vec}_k
            (\mathrm{PrimitiveIntro}\;n)
        \end{align*}
    \end{definition}

    \begin{definition}[Regra de eliminação primitiva]
        \label{def:regra-de-eliminação-primitiva}
        Uma \strong{regra de eliminação primitiva} é definida por uma aridade $n
        : \mathbb{N}$, por um número de hipóteses extras $h : \mathbb{N}$, por
        um número de proposições extras $p : \mathbb{N}$, por uma escolha de
        conclusão $i : \mathrm{Fin}_{n+p}$, por uma escolha de proposições para
        as hipóteses extras $\function \pi h {n+p}$ e por um cabeçalho
        $\function \sigma h {\mathbb{J}}$. É entendida como a regra de
        inferência
        \[
            \inferrule {
                \Gamma \vdash K(A_1, \cdots, A_{n-1})\;\mathsf{true} \\
                \Gamma \vdash A_{\pi(1)} \sigma(1)\\
                \Gamma \vdash A_{\pi(h-1)} \sigma(h-1)
            } {
            }
        \]

        Ou seja,

        \begin{align*}
            &\mathrm{PrimitiveElim} : \mathbb{N} \rightarrow \mathrm{Type} \\
            &\mathrm{PrimitiveElim}\;n = \sum_{h,p : \mathbb{N}}
                \mathrm{Fin}_{n+p} \times
                (\mathrm{Fin}_{h} \rightarrow \mathrm{Fin}_{n+p}) \times
                (\mathrm{Fin}_{h} \rightarrow \mathbb{J})
        \end{align*}

    \end{definition}

    \begin{definition}[Regumento Negativo]
        \label{def:regulamento-negativo}
        Um \strong{regulamento negativo} é uma sequência de regras de eliminação
        primitivas de mesma aridade. Ou seja,
        \begin{align*}
            &\mathrm{NegativeRuleset} : \mathrm{Type} \\
            &\mathrm{NegativeRuleset} = \sum_{n,k : \mathbb{N}} \mathrm{Vec}_k
            (\mathrm{PrimitiveElim}\;n)
        \end{align*}
    \end{definition}

    Uma vez definidos os regulamentos (que moralmente definem conectivos -- isto
    é, dão a \emph{assinatura} da lógica), definimos as proposições que podemos
    construir com esses regulamentos (conectivos). Precisamos de um
    \strong{regulamento}, em geral

    \begin{definition}[Regulamento]
        Um \strong{regulamento} é uma coleção de regulamentos positivos e
        negativos.
        \begin{align*}
            &\mathrm{Ruleset} : \mathrm{Type} \\
            &\mathrm{Ruleset} = \sum_{m : \mathbb{N}} \mathrm{Fin}_m \rightarrow
            (\mathrm{PositiveRuleset} + \mathrm{NegativeRuleset})
        \end{align*}
    \end{definition}

    \begin{definition}[Proposições sobre regulamentos]
        \label{def:proposições-sobre-regulamentos}
        \begin{align*}
            \mathrm{Prop} &: \mathrm{Ruleset} \rightarrow \mathrm{Type} \\
            \mathrm{mkProp} &: \sum_{\mathcal{R} : \mathrm{Ruleset}}
            \sum_{i : \mathrm{Fin}_{\pi_L\;\mathcal{R}}}
            \mathrm{Vec}_{\mathrm{arity}(\pi_R\;\mathcal{R}\;i)}
            (\mathrm{Prop}_{\mathcal{R}}) \rightarrow
            \mathrm{Prop}_{\mathcal{R}}
        \end{align*}
        onde $\mathrm{arity}(\mathrm{In}_{\mathrm{L}}R) = \pi_{\mathrm{L}}\;R$,
        e $\mathrm{arity}(\mathrm{In}_{\mathrm{R}}R) = \pi_{\mathrm{L}}\;R$
    \end{definition}

    Mas, como desenvolvemos antes, a lógica é feita sobre julgamentos, e não
    proposições. Julgamentos básicos são, por enquanto, de dois tipos: aqueles
    em que afirmamos que uma proposição é bem-formada -- $A\;\mathrm{prop}$ --
    ou então verdadeira -- $A\;\mathrm{true}$. A classe de julgamentos sobre um
    regulamento $\mathcal{R}$, então, é

    \[
        \mathrm{Prop}_{\mathcal{R}} \times \mathbb{J}
    \]

    Claro que agora desejamos definir os julgamentos hipotéticos:

    \begin{align*}
        &\mathrm{HypotheticalJudgement} : \mathrm{Ruleset} \rightarrow \mathrm{Type} \\
        &\mathrm{HypotheticalJudgement} \mathcal{M} =
        [\mathrm{Prop}_{\mathcal{R}}] \times
        \mathrm{Prop}_{\mathcal{R}}
    \end{align*}

    Denotamos um julgamento hipotético $(\Gamma, J)$ por $\Gamma \vdash J$.

    Agora, por fim, podemos definir o sistema \foreign{per se}, o \strong{tipo
    das provas}.

    \[
        \mathrm{NaturalProof} : \prod_{\mathcal{M} : \mathrm{Ruleset}}
                                 \mathrm{HypotheticalJudgement} \rightarrow
                                 \mathrm{Type} \\
    \]

    Primeiro, definimos os constutores estruturais. A reflexividade diz que
    sempre se deduz que $A$ é verdadeiro de que $A$ é verdadeiro:

    \begin{align*}
        \mathrm{refl} : &\prod_{P : \mathrm{Prop}_{\mathcal{R}}} \\
                        &\mathrm{NaturalProof}_{\mathcal{M}}\;(\Gamma \vdash P\;\mathsf{prop}) \rightarrow \\
                        &\mathrm{NaturalProof}_{\mathcal{M}}\;(\Gamma, P\;\mathsf{true} \vdash P\;\mathsf{true}) \\
    \end{align*}

    Enfraquecimento expressa a ideia de que uma inferência permanece válida ao
    se adicionar hipóteses:

    \begin{align*}
        \mathrm{weak} : &\prod_{P : \mathrm{Prop}_{\mathcal{R}}} \\
                        &\prod_{J : \mathrm{AtomicJudgement}\;\mathcal{M}} \\
                        &\mathrm{NaturalProof}_{\mathcal{M}}\;(\Gamma \vdash J) \rightarrow \\
                        &\mathrm{NaturalProof}_{\mathcal{M}}\;(\Gamma \vdash P\;\mathsf{prop}) \rightarrow \\
                        &\mathrm{NaturalProof}_{\mathcal{M}}\;(\Gamma, P\;\mathsf{true} \vdash J)
    \end{align*}


    O primeiro construtor lógico é a aplicação de uma regra de introdução primitiva

    \begin{align*}
    \end{align*}

\end{document}
