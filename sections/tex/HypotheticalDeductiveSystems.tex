\documentclass[main.tex]{subfiles}

\begin{document}

    \section{Sistemas dedutivos hipotéticos}

        Os próximos passos no nosso trabalho com sistemas dedutivos é
        \emph{especializá-los}. Afinal, assim como os definimos, são demasiado
        genéricos. Os tipos de resultados que podemos provar sobre eles são
        restritos, e afinal queremos explorar tipos mais específicos de sistemas
        dedutivos.

        A primeira especialização que exploraremos é a dos \strong{sistemas
        dedutivos hipotéticos} -- aqueles que correspondem melhor à Dedução
        Natural, pois contam com a adição de \strong{contextos}. Intuitivamente,
        um contexto é muito familiar para o matemático comum. Sempre que
        provamos um resultado do tipo ``para todo $x \in A$, vale a propriedade
        $\varphi$'', quase que imediatamente isso se segue de ``seja $x \in
        A$'', sucedido pela prova em si. Esse procedimento (de \emph{declaração}
        de variáveis) é a movimentação de uma variável presa para o contexto. O
        \emph{objetivo}, que é a sentença $\forall x \in A \mathrel. \varphi$,
        se torna $\varphi$. Mas agora temos acesso a esse objeto $x$ de $A$. A
        variável deixa de estar presa na \emph{teoria} e passa a estar presa na
        \emph{metateoria}.

        Então o que é, formalmente, um contexto? A primeira coisa a se perceber
        é o fato de que se trata de uma coleção de julgamentos. Por exemplo,
        considere o Teorema da Dedução:

        \[
            \infer {
                \Gamma, A \true \vdash B \true
            } {
                \Gamma \vdash A \rightarrow B \true
            }
        \]

        A leitura que fizemos anteriormente se aplica ao ler a regra de baixo
        para cima. ``Para provar que $A$ implica em $B$, basta provar $B$ tendo
        $A$ como hipótese''. Essa \emph{tática}\footnotemark de prova permite
        transformar um objetivo $A \rightarrow B \true$ em $B \true$,
        adicionando o \emph{julgamento} $A \true$ no contexto.

        \footnotetext{} % REVER

        A segunda coisa que precisamos observar é que contextos tem
        \emph{ordem}. Considere o seguinte exemplo:

        \begin{quotation}
            Para todo ponto $x$ em uma variedade $\mathcal{M}$, e para todo vetor $v$ no
            espaço tangente $T_x\mathcal{M}$, vale a propriedade $\varphi$.
        \end{quotation}

        O importante a se observar é o fato de que a variável $v$ tem um tipo
        que \emph{depende} de $x$. Provar essa afirmação em geral começaria de
        alguma forma similar a

        \begin{quotation}
            Seja $x \in \mathcal{M}$ e $v \in T_x\mathcal{M}$. Então \omissis.
        \end{quotation}

        Fazendo uma análise similar à de antes, neste caso, chegamos ao
        seguinte:

        \begin{itemize}
            \item ``Seja $x \in \mathcal{M}$'': Substituímos $\forall x \in
                \mathcal{M} \forall v \in T_x\mathcal{M} \mathrel. \varphi$ por
                $\forall v \in T_x\mathcal{M} \mathrel. \varphi$, adicionando
                $x$ do conjunto $\mathcal{M}$ no contexto. Note que
                $T_x\mathcal{M}$ ainda faz sentido, pois mantemos $x$
                \foreign{``in the back of my mind''}.

            \item ``e $v \in T_x\mathcal{M}$'': Substituímos o objetivo $\forall
                v \in T_x\mathcal{M} \mathrel. \varphi$ por $\varphi$,
                adicionando $v$ do espaço tangente no contexto. Qualquer
                instancia livre de $x$ e $v$ em $\varphi$ \emph{faz sentido},
                porque $x$ e $v$ estão no contexto, que pode ser consultado a
                qualquer momento.
        \end{itemize}

        Mas o que acontenceria se tentássemos, por algum capricho, declarar $v$
        antes de $x$? O objetivo se tornaria $\forall x \mathrel. \varphi x$,
        com $v$ do espaço tangente de $x$ no contexto. Mas não existe nenhum
        objeto chamado $x$! A variável $x$ está presa no julgamento que queremos
        provar, e por isso não há $x$ no contexto. Esse é o tipo de coisa que
        numa palestra pode levar alguem a pensar (talvez não num exemplo tão
        simples) ``mas quem é $x$?''. Afinal, considere, o objetivo poderia
        muito bem ser $\forall y \mathrel. \varphi[x \coloneqq y]$. A conclusão
        é que a declaração $x \in \mathcal{M}$ \emph{precede} a declaração $v
        \in T_x\mathcal{M}$ (e, evidentemente, ``seja $\mathcal{M}$ variedade''
        precede as duas).

        Então contextos são coleções de julgamentos com ordem. Um ponto final,
        embora menos sustentado aqui, é que será conveniente que contextos sejam
        finitos, de tamanho bem definido. Isso se relaciona com a ideia de que
        quando declaramos uma variável, adicionamos \emph{um} julgamento ao
        contexto, e sem tamanho se torna o \emph{sucessor} do tamanho anterior
        (e isso é um dos construtores dos números naturais). A estrutura ideal
        para o que estamos descrevendo é o \strong{vetor}, no jargão de
        programação com tipos dependentes. Como vetores tem tamanho, definimos
        os contextos para carregar os tamanhos consigo.

        \begin{definition}[Contexto]
            Um \strong{contexto} sobre um tipo de julgamentos $\mathcal{J}$ é um
            número natural $n : \mathbb{N}$ acompanhado de um vetor de tamanho
            $n$.
            \begin{code}
                >>> Context
            \end{code}
        \end{definition}

        O leitor mais atento pode perceber que já nos deparamos com contextos
        antes. Contextos, nesse sentido, são os objetos que vivem no lado
        esquerdo das inferências. Isso não é uma coincidência: a nossa
        especialização de sistemas dedutivos vai exatamente nessa direção.
        Pretendemos trocar

        \[
            \infer {
                \mathcal{J}_1 \\
                \cdots \\
                \mathcal{J}_n
            } {
                \mathcal{J}
            }
        \]

        por

        \[
            \infer {
                \Gamma_1 \vdash \mathcal{J}_1 \\
                \cdots \\
                \Gamma_n \vdash \mathcal{J}_n
            } {
                \Gamma \vdash \mathcal{J}
            }
        \]

        Ou seja, os objetos sobre os quais construímos provas agora
        \emph{também} são inferências.

        \begin{definition}[Regulamento hipotético]
            Um \strong{regulamento hipotético} sobre um tipo $\mathcal{J}$ é um
            regulamento sobre as inferências em $J$.

            \begin{code}
                >>> HypotheticalRuleset
            \end{code}
        \end{definition}

\end{document}
